
// MadLib
// Your Name

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

//Prototype
void MadLibPrint(string MadLib);

int main()
{
	//St. Patricks Themed Mad Lib -- https://www.woojr.com/st-patricks-day-ad-libs-for-kids/st-patricks-wear-green/

	string MadLib[16]; //Starts at zero otherwise would be 16 total.

	cout << "Enter a Month: ";
	getline(cin, MadLib[0]);
	cout << "Enter a verb (-ing): ";
	getline(cin, MadLib[1]);
	cout << "Enter a color: ";
	getline(cin, MadLib[2]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[3]);
	cout << "Enter a noun: ";
	getline(cin, MadLib[4]);
	cout << "Enter a color: ";
	getline(cin, MadLib[5]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[6]);
	cout << "Enter a adverb: ";
	getline(cin, MadLib[7]);
	cout << "Enter a plural noun: ";
	getline(cin, MadLib[8]);
	cout << "Enter a article of clothing: ";
	getline(cin, MadLib[9]);
	cout << "Enter a verb: ";
	getline(cin, MadLib[10]);
	cout << "Enter a color: ";
	getline(cin, MadLib[11]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[12]);
	cout << "Enter a number: ";
	getline(cin, MadLib[13]);
	cout << "Enter a noun: ";
	getline(cin, MadLib[14]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[15]);

	//Array is not being passed to the method // Lookup how to pass array through including all 
	//for (int i = 0; i <= 16; i++)
	//{	
	//	MadLibPrint(MadLib[i]);
	//}
	

	(void)_getch();
	return 0;
}


void MadLibPrint(string MadLib) 
{
	//Print the array with the Madlib message here // ex: cout << MadLib[1];
	
	for (int i = 0; i <= 16; i++)
	{
		cout << MadLib[i];
	}

}